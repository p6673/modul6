<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Add Data</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-4 my-5">
        <div class="row">
            <div class="col"></div>
            <div class="col-12 col-md-6 py-3 d-flex justify-content-center">
                <div class="bg12 p-5 text-center w-100 r40">
                    <a href="{{ url('/') }}" class="float-start"><img src="{{ asset('img/back.png') }}"
                            alt=""></a>
                    <h1 class="fs-2 cl1 f500 mb-5">Add New Data</h1>
                    <p class="cl18 f500 float-start ps-4">Account</p>
                    <form action="" method="POST" id="myform">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="email"
                            placeholder="Email">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" id="password"
                            name="password" placeholder="Password">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="retype"
                            placeholder="Retype">
                        <br>
                        <p class="cl18 f500 float-start ps-4 pt-4">Detail</p>
                        <input type="number" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="nim"
                            placeholder="NIM" minlength="12">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="nama"
                            placeholder="Nama">
                        <input type="number" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;"
                            name="phone" placeholder="Phone">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;"
                            name="tmptLhr" placeholder="Place of Birth">
                        <input type="date" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="tglLhr"
                            placeholder="Date of Birth">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;"
                            name="address" placeholder="Address">
                        <select name="role" class="border-0 w-100 bg12 r12 cl1 f300 py-2 px-4 mb-4">
                            <option class="cl3 py-2" selected>User</option>
                            <option class="cl3 py-2">Administrator</option>
                        </select>
                        <button type="submit" class="btn mb-4 f500 py-3 r12 bg0 cl3 w-100">Submit</button>
                    </form>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
        $("#myform").validate({
            errorPlacement: (err, e) => {
                err.insertBefore(e);
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                retype: {
                    required: true,
                    equalTo: "#password"
                },
                nim: {
                    required: true,
                    minlength: 12,
                    maxlength: 12
                },
                nama: {
                    required: true,

                },
                phone: {
                    required: true,

                },
                tmptLhr: {
                    required: true,

                },
                tglLhr: {
                    required: true,

                },
                address: {
                    required: true,

                },
                role: {
                    required: true,

                },
            },
        });
    </script>
</body>

</html>
