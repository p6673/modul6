<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Change Password</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-4 my-5">
        <div class="row">
            <div class="col"></div>
            <div class="col-12 col-md-6 py-3 d-flex justify-content-center">
                <div class="bg12 p-5 text-center w-100 r40">
                    <a href="{{ url('/profile') }}" class="float-start"><img src="{{ asset('img/back.png') }}" alt=""></a>
                    <h1 class="fs-2 cl1 f500 mb-5">Change Password</h1>
                    <form action="../controller/controller.php?d=changePassword" method="POST">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mt-3 mb-5" style="height: 50px;"
                            name="old" placeholder="Old Password">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="new"
                            placeholder="Password">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-5" style="height: 50px;" name="retype"
                            placeholder="Retype">
                        <button type="submit" class="btn mb-4 f500 py-3 r12 bg0 cl3 w-100">Submit</button>
                    </form>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</body>

</html>
