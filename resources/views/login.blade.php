<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-4 my-5">
        <div class="row">
            <div class="col-12 col-md-6 py-5">
                <div style="width: 330px;">
                    <h1 class="cl1 f500" style="font-size: 70px;">Modul 5</h1>
                    <p class="fs-7 cl1 f300 ">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti labore
                        ducimus, hic error consequatur cum incidunt temporibus sapiente ut nisi blanditiis in esse
                        fugiat harum illum similique iste dolorem sunt.</p>
                    <button class="btn py-3 fs-6 cl18 f500 r12 w-100 mt-4"
                        style="background: linear-gradient(96.7deg, #F7B130 -6.48%, #EE5327 85.34%);">Register</button>
                </div>
            </div>
            <div class="col-12 col-md-6 py-3 d-flex justify-content-center">
                <div class="bg12 p-5 text-center r40" style="width: 450px;">
                    <h1 class="fs-1 cl1 f500 mb-5">Login</h1>
                    <form action="{{ url('api/') }}" method="POST">
                        <input type="text" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="email"
                            placeholder="Email">
                        <input type="password" class="w-100 bg12 r12 cl1 f300 px-4 mb-5" style="height: 50px;"
                            name="password" placeholder="Password">
                        <button type="submit" class="btn mb-4 f500 py-3 r12 bg0 cl3 w-100">Login</button>
                    </form>
                    <a href="{{ url('/forgot-password', []) }}" class="btn cl17 f500">Forgot Password ?</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
