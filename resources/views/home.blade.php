<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Home</title>
    <link rel="stylesheet" href="{{ asset('dt/datatables.css') }}">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-5">
        <div class="row pb-5">
            <div class="col-12 col-md-6">
                <div style="width: 330px;">
                    <h1 class="cl1 f500 fs-1">Modul 6</h1>
                </div>
            </div>
            <div class="col-12 col-md-6 d-flex justify-content-end">
                <div class="bg2 py-3 px-4 r12 d-flex justify-content-center">
                    <h5 class="m-0 fs-5 cl3 f500 align-self-center pe-5">{{ $username }}</h5>
                    <a class="me-3" href="{{ url('profile') }}"><img src="{{ asset('img/profile.png') }}"
                            alt=""></a>
                    <a href="{{ url('/login') }}" onclick="return confirm('Are you sure?')"><img
                            src="{{ asset('img/logout.png') }}" alt=""></a>
                </div>
            </div>
        </div>
        <main class="bg12 p-5 r40">
            <h1 class="fs-2 cl1 f500 text-center">User List</h1>
            <a href="{{ url('/add') }}" class="px-5 py-2 bg0 cl3 f500 r12 btn mb-5">Add</a>
            <table id="mcontent" class="display w-100 text-center mb-3" style="border-spacing: 0;">
                <thead class="bg2 cl3">
                    <tr>
                        <th class="f500 py-3 ps-4">NIM</th>
                        <th class="f500">Nama</th>
                        <th class="f500">Email</th>
                        <th class="f500">Phone</th>
                        <th class="f500">Role</th>
                        <th class="f500 pe-4">Action</th>
                    </tr>
                </thead>
                <br>
                <tbody>
                    @foreach ($data as $d)
                        <tr class="r12 f300">
                            <td class="ps-4 py-2"> {{ $d['nim'] }} </td>
                            <td> {{ $d['nama'] }} </td>
                            <td> {{ $d['email'] }} </td>
                            <td> {{ $d['phone'] }} </td>
                            <td> {{ $d['role'] }} </td>
                            <td class="pe-4 text-center">
                                <a href="{{ url('/edit/' . $d['nim']) }}"><img src="{{ asset('img/edit.png') }}"
                                        alt=""></a>
                                <a href="{{ url('/delete/' . $d['nim']) }}"
                                    onclick="return confirm('Are you sure?')"><img src="{{ asset('img/trash.png') }}"
                                        alt=""></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </main>
    </div>
</body>
<script src="{{ asset('dt/datatables.js') }}"></script>
<script>
    $(document).ready(() => $('#mcontent').DataTable())
</script>

</html>
