<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Profile</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-4 my-5">
        <div class="bg12 p-5 text-center w-100 r40">
            <div class="ps-3 py-3 pe-5">
                <a href="{{ url('/') }}" class="float-start"><img src="{{ asset('img/back.png') }}" alt=""></a>
                <h1 class="fs-2 cl1 m-0 f500 mb-5">Profile</h1>
                <div class="row">
                    <div class="col-12 col-lg-3 py-3 d-flex justify-content-center">
                        <img src="{{ asset('img/bigProfile.png') }}" height="156" width="156" alt="">
                    </div>
                    <div class="col-12 col-lg-9 py-3">
                        <div class="bg2 p-4 w-100 r12 mb-5">
                            <h1 class="fs-4 mb-0 f500 cl3 text-start">Achmad Reza Fahlevi</h1>
                            <p class="fs-6 f300 text-start m-0 cl18">achmadreza993@gmail.com</p>
                        </div>
                        <div class="bg12 r12 p-4">
                            <div class="row text-start">
                                <div class="col-12 col-md-6">
                                    <span class="fs-8 cl18 f500">NIM</span>
                                    <p class="fs-6 cl18 f300">210535614841</p>
                                    <span class="fs-8 cl18 f500">Tempat Lahir</span>
                                    <p class="fs-6 cl18 f300">Sidoarjo</p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <span class="fs-8 cl18 f500">Phone</span>
                                    <p class="fs-6 cl18 f300">082333512476</p>
                                    <span class="fs-8 cl18 f500">Tempat Lahir</span>
                                    <p class="fs-6 cl18 f300">Sidoarjo</p>
                                </div>
                                <div class="col-12 pt-3">
                                    <span class="fs-8 cl18 f500">Address</span>
                                    <p class="fs-7 cl18 f300">Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                                        Repellat blanditiis odio harum, perspiciatis possimus vel tempore officiis dolor
                                        tenetur, assumenda rem rerum, iure minus minima molestiae recusandae facere
                                        ullam labore.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mt-5 px-5 text-start">
                        <a href="{{ url('/change-password') }}" class="btn cl3 f500">Change Password</a>
                    </div>
                    <div class="col-12 col-md-6 mt-5 text-end">
                        <a href="{{ url('/login') }}" class="btn f500"
                            style="color:rgba(255,91,91,.8)">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
