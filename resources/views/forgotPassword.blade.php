<?php
if (isset($_GET['s'])) {
    echo $_GET['s'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <title>Forgot Password</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body>
    <div class="container py-4 my-5">
        <div class="row">
            <div class="col"></div>
            <div class="col-12 col-md-6 py-3 d-flex justify-content-center">
                <div class="bg12 p-5 text-center w-100 r40">
                    <a href="{{ url('/login') }}" class="float-start"><img src="{{ asset('img/back.png') }}"
                            alt=""></a>
                    <h1 class="fs-2 cl1 f500 mb-5">Forgot Password</h1>
                    <form action="../controller/controller.php?d=forgotPassword" method="POST">
                        <input type="number" class="w-100 bg12 r12 cl1 f300 px-4 mb-4" style="height: 50px;" name="nim"
                            placeholder="NIM">
                        <p class="cl18 f500 fs-7 mb-5">Please enter the NIM registered on your account. We will send a
                            link to change your password to the email registered to your account.</p>
                        <button type="submit" class="btn mb-4 f500 py-3 r12 bg0 cl3 w-100">Submit</button>
                    </form>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</body>

</html>
