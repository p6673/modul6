<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home', [
        "data" => [
            [
                "nim" => "1",
                "nama" => "1",
                "email" => "1",
                "phone" => "1",
                "role" => "1",
            ],
            [
                "nim" => "2",
                "nama" => "2",
                "email" => "2",
                "phone" => "2",
                "role" => "2",
            ],
            [
                "nim" => "3",
                "nama" => "3",
                "email" => "3",
                "phone" => "3",
                "role" => "3",
            ],
        ],
        "username" => "Achmad Reza"
    ]);
});

Route::get('/login', function () {
    return view('login');
});
Route::get('/forgot-password', function () {
    return view('forgotPassword');
});
Route::get('/profile', function () {
    return view('profile');
});
Route::get('/change-password', function () {
    return view('changePassword');
});
Route::get('/add', function () {
    return view('add');
});
Route::get('/edit/{nim}', function ($nim) {
    return view('edit', ['data' => [
        "email" => "email" . $nim . "@gmail.com",
        "password" => "password" . $nim,
        "nim" => $nim,
        "nama" => "Nama " . $nim,
        "phone" => "08" . $nim,
        "tmptLhr" => "Tempat Lahir " . $nim,
        "tglLhr" => "2000-01-01",
        "address" => "Address" . $nim,
        "role" => "Administrator",
    ]]);
});
